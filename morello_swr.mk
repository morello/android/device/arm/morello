# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

$(call inherit-product, $(LOCAL_PATH)/full.mk)

PRODUCT_NAME := morello_swr
PRODUCT_BRAND := Android
PRODUCT_MANUFACTURER := Arm
PRODUCT_DEVICE := morello
PRODUCT_MODEL := Morello SWR

# Enable libshim for morello_swr by default
TARGET_USE_LIBSHIM := true

PRODUCT_REQUIRES_INSECURE_EXECMEM_FOR_SWIFTSHADER := true

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.morello_swr.rc:$(TARGET_COPY_OUT_ROOT)/init.morello_swr.rc \

PRODUCT_PACKAGES += \
    libEGL_swiftshader \
    libGLESv1_CM_swiftshader \
    libGLESv2_swiftshader \

PRODUCT_PROPERTY_OVERRIDES += \
    ro.hardware.egl=swiftshader \
