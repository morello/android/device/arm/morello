# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit_only.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# https://source.android.com/devices/tech/ota/dynamic_partitions/implement
# https://source.android.com/devices/tech/ota/dynamic_partitions/implement#adb-remount
PRODUCT_USE_DYNAMIC_PARTITIONS := false

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fstab.morello:$(TARGET_COPY_OUT_RAMDISK)/fstab.morello \
    $(LOCAL_PATH)/fstab.morello:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.morello \
    $(LOCAL_PATH)/init.morello.rc:$(TARGET_COPY_OUT_ROOT)/init.morello.rc \
    $(LOCAL_PATH)/init.morello_fvp.rc:$(TARGET_COPY_OUT_ROOT)/init.morello_fvp.rc \
    $(LOCAL_PATH)/init.morello_soc.rc:$(TARGET_COPY_OUT_ROOT)/init.morello_soc.rc \

PRODUCT_PACKAGES += \
    libandroidicu.com.android.art.debug:purecap \
    libandroidicu.com.android.art.release:purecap \
    libc.bootstrap:purecap \
    libc.com.android.runtime:purecap \
    libdl.bootstrap:purecap \
    libdl.com.android.runtime:purecap \
    libdl_android.bootstrap:purecap \
    libdl_android.com.android.runtime:purecap \
    libicui18n.com.android.art.debug:purecap \
    libicui18n.com.android.art.release:purecap \
    libicuuc.com.android.art.debug:purecap \
    libicuuc.com.android.art.release:purecap \
    libm.bootstrap:purecap \
    libm.com.android.runtime:purecap \
    libnativehelper.com.android.art.debug:purecap \
    libnativehelper.com.android.art.release:purecap \
    linker.com.android.runtime:purecap \
    linker:purecap \
    init_second_stage:purecap \
    logd:purecap \
    servicemanager:purecap \
    toybox:purecap \

PRODUCT_PACKAGES += \
    toggle_mti

PRODUCT_MODULES_ALLOW_NON_EXISTENT_SRC_PATHS += \
    libclang_rt.scudo_minimal-aarch64-android \
    libclang_rt.scudo_minimal-aarch64-android.static \
    libclang_rt.scudo-aarch64-android \
    libclang_rt.scudo-aarch64-android.static
