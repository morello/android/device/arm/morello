# Copyright (c) 2020-2021 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RECOVERY := true

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-2a_with_caps
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := arm64-v8a

TARGET_2ND_ARCH := morello
TARGET_2ND_CPU_VARIANT := generic
TARGET_2ND_CPU_ABI := morello

TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

BOARD_BUILD_SYSTEM_ROOT_IMAGE := false
BOARD_USES_RECOVERY_AS_BOOT := false

# Workaround: these lines are just to please fs_config_generator.py
# which is passed "--all-partitions $(fs_config_generate_extra_partition_list)".
# The variable would be empty otherwise triggering an argument parsing error.
# OEMIMAGE has no other side-effects in the build system, hope it stays so.
# See build/make/tools/fs_config/Android.mk for details.
BOARD_USES_OEMIMAGE := true
BOARD_OEMIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_SEPOLICY_DIRS += device/generic/goldfish/fvpbase/sepolicy

ifeq (,$(filter morello_nano%, $(TARGET_PRODUCT)))
    # any but nano
    BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1342177280   # 1.25 GB
else
    BOARD_SYSTEMIMAGE_PARTITION_SIZE := 268435456    # 256 MB
endif
BOARD_USERDATAIMAGE_PARTITION_SIZE := 536870912      # 512 MB
