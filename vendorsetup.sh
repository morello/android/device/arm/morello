# Copyright (c) 2020-2021 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

MORELLO_VENDORSETUP_STATUS="0"

__ANDROID_TREE_ROOT=$(gettop)
__REPO_ROOT=$(realpath "${__ANDROID_TREE_ROOT}/..")

__SYMLINK_BASE_PATH="${__ANDROID_TREE_ROOT}/prebuilts/clang/host/linux-x86"
__CLANG_MORELLO_SYMLINK_NAME="clang-morello"

__MORELLO_LLVM_PATH="${__REPO_ROOT}/tools/clang"

# MORELLO_LLVM_PATH
if [[ -v MORELLO_LLVM_PATH && "${MORELLO_LLVM_PATH}" != "${__MORELLO_LLVM_PATH}" ]]; then
    echo "*** Warning *** vendorsetup.sh: MORELLO_LLVM_PATH is already set and won't be overridden."

    __MORELLO_LLVM_PATH="${MORELLO_LLVM_PATH}"
fi

if ! test -f "${__MORELLO_LLVM_PATH}/bin/clang"; then
        echo "*** Error *** vendorsetup.sh: '${__MORELLO_LLVM_PATH}'" >&2
        echo "*** Error *** vendorsetup.sh: doesn't seem to point to an LLVM toolchain directory," >&2
        echo "*** Error *** vendorsetup.sh: which might be because the toolchain hadn't been downloaded/unpacked correctly" >&2
        echo "*** Error *** vendorsetup.sh: or MORELLO_LLVM_PATH contains a wrong path." >&2
        MORELLO_VENDORSETUP_STATUS="1"
else
    export MORELLO_LLVM_PATH="${__MORELLO_LLVM_PATH}"
    echo "export MORELLO_LLVM_PATH=\"${MORELLO_LLVM_PATH}\""

    ln -snf "${MORELLO_LLVM_PATH}" "${__SYMLINK_BASE_PATH}/${__CLANG_MORELLO_SYMLINK_NAME}"

    __LLVM_PREBUILTS_VERSION="${__CLANG_MORELLO_SYMLINK_NAME}"
    __LLVM_RELEASE_VERSION="$(cd "${__SYMLINK_BASE_PATH}/${__LLVM_PREBUILTS_VERSION}/lib64/clang" > /dev/null; echo *)"

    # LLVM_PREBUILTS_VERSION
    if [[ -v LLVM_PREBUILTS_VERSION && "${LLVM_PREBUILTS_VERSION}" != "${__LLVM_PREBUILTS_VERSION}" ]]; then
        echo "*** Warning *** vendorsetup.sh: LLVM_PREBUILTS_VERSION will be overridden. The current value is \"${LLVM_PREBUILTS_VERSION}\""
    fi

    export LLVM_PREBUILTS_VERSION="${__LLVM_PREBUILTS_VERSION}"
    echo "export LLVM_PREBUILTS_VERSION=\"${LLVM_PREBUILTS_VERSION}\""

    # LLVM_RELEASE_VERSION
    if [[ -v LLVM_RELEASE_VERSION && "${LLVM_RELEASE_VERSION}" != "${__LLVM_RELEASE_VERSION}" ]]; then
        echo "*** Warning *** vendorsetup.sh: LLVM_RELEASE_VERSION is already set and won't be overridden."

        __LLVM_RELEASE_VERSION="${LLVM_RELEASE_VERSION}"
    fi

    export LLVM_RELEASE_VERSION="${__LLVM_RELEASE_VERSION}"
    echo "export LLVM_RELEASE_VERSION=\"${LLVM_RELEASE_VERSION}\""

    if ! test -d "${__SYMLINK_BASE_PATH}/${LLVM_PREBUILTS_VERSION}/lib64/clang/$LLVM_RELEASE_VERSION/"; then
            echo "*** Error *** vendorsetup.sh: Unexpected clang directory layout: ${__SYMLINK_BASE_PATH}/${LLVM_PREBUILTS_VERSION}/lib64/clang" >&2
            echo "*** Error *** vendorsetup.sh:   You might need to unset MORELLO_LLVM_PATH, LLVM_PREBUILTS_VERSION and LLVM_RELEASE_VERSION variables" >&2
            echo "*** Error *** vendorsetup.sh:   and re-source build/envsetup.sh" >&2
            MORELLO_VENDORSETUP_STATUS="1"
    fi
fi

unset __ANDROID_TREE_ROOT
unset __REPO_ROOT

unset __SYMLINK_BASE_PATH
unset __CLANG_MORELLO_SYMLINK_NAME

unset __MORELLO_LLVM_PATH
unset __LLVM_PREBUILTS_VERSION
unset __LLVM_RELEASE_VERSION
