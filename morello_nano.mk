# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

$(call inherit-product, $(LOCAL_PATH)/morello.mk)
$(call inherit-product, device/generic/goldfish/minimal_system.mk)

PRODUCT_NAME := morello_nano
PRODUCT_BRAND := Android
PRODUCT_MANUFACTURER := Arm
PRODUCT_DEVICE := morello
PRODUCT_MODEL := Morello Nano

# Enable libshim for morello_nano by default
TARGET_USE_LIBSHIM := true

# The check would fail because there are no boot jars.
SKIP_BOOT_JARS_CHECK ?= true

# Support for reduced checkout where some projects are not present,
# and so are some targets which aren't needed in nano configuration.
ALLOW_MISSING_DEPENDENCIES := true
TARGET_SKIP_CURRENT_VNDK := true

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.morello_nano.rc:$(TARGET_COPY_OUT_ROOT)/init.morello_nano.rc \

# Don't use the precollected boot image profile for generating boot image
PRODUCT_USE_PROFILE_FOR_BOOT_IMAGE := false

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    dalvik.vm.image-dex2oat-Xms=64m \
    dalvik.vm.image-dex2oat-Xmx=64m \
    dalvik.vm.dex2oat-Xms=64m \
    dalvik.vm.dex2oat-Xmx=512m \
    ro.dalvik.vm.native.bridge=0 \
    dalvik.vm.usejit=true \
    dalvik.vm.usejitprofiles=true \
    dalvik.vm.appimageformat=lz4

PRODUCT_PACKAGES += \
    com.android.art \
    com.android.runtime \
    crash_dump \
    gdbserver \
    grep \
    init_vendor \
    ip \
    ping \
    selinux_policy_nonsystem \
