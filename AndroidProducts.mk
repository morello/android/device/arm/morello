# Copyright (c) 2020-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/morello_nano.mk \
    $(LOCAL_DIR)/morello_swr.mk \
    $(LOCAL_DIR)/morello_nano_nolibshim.mk \
    $(LOCAL_DIR)/morello_swr_nolibshim.mk \
    $(LOCAL_DIR)/morello_nano_libshim.mk \
    $(LOCAL_DIR)/morello_swr_libshim.mk \

COMMON_LUNCH_CHOICES := \
    morello_nano-eng \
    morello_swr-eng \
    morello_nano_nolibshim-eng \
    morello_swr_nolibshim-eng \
    morello_nano_libshim-eng \
    morello_swr_libshim-eng \
