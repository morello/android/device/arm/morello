# Copyright (c) 2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: Apache License 2.0

$(call inherit-product, $(LOCAL_PATH)/morello_swr.mk)

PRODUCT_NAME := morello_swr_libshim
TARGET_USE_LIBSHIM := true
